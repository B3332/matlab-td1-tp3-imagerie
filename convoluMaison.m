function [ imageConvolue ] = convoluMaison( masque, image, p, offset )
[NL,NC] = size(image);
imageConvolue = zeros(NL,NC);

for nl=2:NL-1
    for nc=2:NC-1
        imageConvolue(nl,nc) = offset+sum(sum(image(nl-1:nl+1,nc-1:nc+1).*masque))/p;
        
        if imageConvolue(nl,nc)<0
            imageConvolue(nl,nc)=0;
        end
        if imageConvolue(nl,nc)>255
            imageConvolue(nl,nc)=255;
        end
    end
end

end

