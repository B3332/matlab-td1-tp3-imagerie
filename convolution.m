function test
[im,map]=imread('imageRef.jpg');
%[im,map]=imread('support/grenier.jpg');


%% r�cup�ration des tailles
size(im)
size(map)

%% Rectangle rouge sur l'image. Car on annule les plan 2 et 3
%im(50:100,100:200,2:3)=0;

%% cr�ation de l'image en niveau de gris avec bonnes pond�rations
imndg = 0.3*im(:,:,1) + 0.59*im(:,:,2) + 0.11*im(:,:,3);

%cr�ation de la map pour les nuances de gris
mapndg = ([0:255]/255)'*[1 1 1];
mapndgJaune = ([0:255]/255)'*[1 1 0];

%% Histogramme
[NL,NC] = size(imndg);
histo = zeros(1,256);

for nl=1:NL
    for nc=1:NC
        % On met les valeurs de l'image dans la case correspondante. 
        %On d�cale de 1 pour �viter d'essayer d'acc�der � l'indice 0
        histo(imndg(nl,nc)+1) = histo(imndg(nl,nc)+1) +1;
    end
end

%% histogramme des couleurs
histoR = zeros(1,256);
histoB = zeros(1,256);
histoV = zeros(1,256);

for nl=1:NL
    for nc=1:NC
        % On met les valeurs de l'image dans la case correspondante.
        %On d�cale de 1 pour �viter d'essayer d'acc�der � l'indice 0
        histoR(im(nl,nc,1)+1) = histoR(im(nl,nc,1)+1) +1;
        histoB(im(nl,nc,2)+1) = histoB(im(nl,nc,2)+1) +1;
        histoV(im(nl,nc,3)+1) = histoV(im(nl,nc,3)+1) +1;
    end
end

%% Histogramme cumul� 
histocum = zeros(1,356);
histocum(1)=histo(1)
for n=2:256
   histocum(n)=histocum(n-1)+histo(n);
end

%% seuillage
imseuil = zeros(NL,NC);
seuil=120;

imseuilVincent = imndg>seuil;
imseuilVincent = imseuilVincent* 255;

for nl=1:NL
    for nc=1:NC
        if(imndg(nl,nc)>seuil)
            imseuil(nl,nc)=255;
        end
    end
end

%% recadrage d'histogramme
imndg = double(imndg);
imrecadre = zeros(NL,NC);
mini = min(min(imndg));
maxi = max(max(imndg));
imrecadre = 255*(imndg-mini)/(maxi-mini);

% Histogramme �tir� 
[NL,NC] = size(imndg);
historecadre = zeros(1,256);

imrecadre =floor(imrecadre);
for nl=1:NL
    for nc=1:NC
        % On met les valeurs de l'image dans la case correspondante. 
        %On d�cale de 1 pour �viter d'essayer d'acc�der � l'indice 0
        historecadre(imrecadre(nl,nc)+1) = historecadre(imrecadre(nl,nc)+1) +1;
    end
end

%% Egalisation
imegal = zeros(NL,NC);
%normalisation de l'histograme par division de la derni�re valeur. 
% => entre 0 et 1 finalement.
histocumNormalise = 255*histocum/histocum(256);

for nl=1:NL
    for nc=1:NC
        imegal(nl,nc) = histocumNormalise(imndg(nl,nc)+1);
    end
end

% Calcul Histogramme normalis� 
historenorma = zeros(1,256);

imegal = floor(imegal);
for nl=1:NL
    for nc=1:NC
        % On met les valeurs de l'image dans la case correspondante. 
        %On d�cale de 1 pour �viter d'essayer d'acc�der � l'indice 0
        historenorma(imegal(nl,nc)+1) = historenorma(imegal(nl,nc)+1) +1;
    end
end

%% Contraste
k = 2;
imcontra = zeros(NL,NC);
moy = mean(mean(imndg));

%Rend plus de contraste
imcontr=k*(imndg-moy)+moy;

% Eviter de sortie du range 0-255
for nl=1:NL
    for nc=1:NC
        if imcontr(nl,nc)<0
           imcontr(nl,nc)=0; 
        end
        if imcontr(nl,nc)>255
           imcontr(nl,nc)=255; 
        end
    end
end

%

end

